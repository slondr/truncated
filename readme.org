#+TITLE: truncated.S

This project was the result of my attempt to make the smallest possible ELF binary that still exists successfully. It was pretty small. I've since deleted the binary, though. I don't know why. The assembly source code is a good place to start recreating it.
